//
//  UIImage+ChangeToSize.m
//  NewKroreaCards
//
//  Created by victorLiu on 2017/11/21.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import "UIImage+ChangeToSize.h"

@implementation UIImage (ChangeToSize)
- (UIImage *)changeToSize:(CGSize)size{
    UIGraphicsBeginImageContext(size);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *tempImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tempImage;
}
@end
