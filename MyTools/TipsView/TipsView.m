//
//  TipsVIew.m
//  client
//
//  Created by victorLiu on 2018/1/22.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import "TipsView.h"
#import "UIColor+GetRGBWithHexadecimalColor.h"


@interface TipsView ()
@property (weak, nonatomic) IBOutlet UIImageView *tipsImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHieght;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tipsTopConstaint;
@property (weak, nonatomic) IBOutlet UILabel *tipsTextLabel;
@end
@implementation TipsView

-(void)setTipsType:(TypeRender)tipsType{
    switch (tipsType) {
        case 0:{
            _tipsImageView.image = [UIImage imageNamed:@"noOrderData"];
//            _tipsTextLabel.textColor = [UIColor getRGBWithHexadecimalColor:orderListButtonTitleColor];
            _tipsTextLabel.text = @"暂无未完成订单";
            _tipsTopConstaint.constant = 48;
            _imageWidth.constant = 110;
            _imageHieght.constant = 112;
        }
            break;
            
        default:
            break;
    }
}

-(void)setImageToTop:(CGFloat)imageToTop{
    if (_imageToTop != imageToTop) {
        _imageToTop = imageToTop;
        _tipsTopConstaint.constant = _imageToTop;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
