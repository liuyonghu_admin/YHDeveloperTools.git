//
//  UIView+RemoveAllSubView.m
//  client
//
//  Created by victorLiu on 2018/1/24.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import "UIView+RemoveAllSubView.h"

@implementation UIView (RemoveAllSubView)
- (void)removeAllSubView{
    NSArray *subViewArray = [self subviews];
    for (NSInteger i = 0; i < subViewArray.count; i ++) {
        UIView *v = subViewArray[i];
        [v removeAllSubView];
    }
}
@end
