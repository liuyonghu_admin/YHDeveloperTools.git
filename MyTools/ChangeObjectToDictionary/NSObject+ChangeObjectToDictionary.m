//
//  NSObject+ChangeObjectToDictionary.m
//  NewKroreaCards
//
//  Created by victorLiu on 2017/12/7.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import <objc/runtime.h>
#import "NSObject+ChangeObjectToDictionary.h"

@implementation NSObject (ChangeObjectToDictionary)
- (NSDictionary *)changeToDictionaryWithObject:(NSObject *)object{
    NSMutableArray *allPropertyNames = [NSMutableArray array];
    unsigned int propertyCount = 0;
    objc_property_t *propertys = class_copyPropertyList([object class], &propertyCount);
    
    for (NSInteger i = 0; i < propertyCount; i ++) {
        objc_property_t property = propertys[i];
        const char *propertyName = property_getName(property);
        [allPropertyNames addObject:[NSString stringWithUTF8String:propertyName]];
    }
    
    free(propertys);
    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc]init];
    for (NSString *propertyName in allPropertyNames) {
        //        NSLog(@"key = %@",propertyName);
        //        MARK:自定义配置
        if ([object valueForKey:propertyName]) {
            if ([propertyName isEqualToString:@"keyForId"]) {
                [tempDic setObject:[object valueForKey:propertyName] forKey:@"id"];
                continue;
            }
            [tempDic setObject:[object valueForKey:propertyName] forKey:propertyName];
        }
        
    }
    //    NSLog(@"dic  == = %@",tempDic);
    return tempDic;
}

@end
