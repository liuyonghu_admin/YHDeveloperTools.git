//
//  UIAlertController+customAlertView.m
//  境外易购
//
//  Created by victorLiu on 2017/11/10.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//

#import "UIAlertController+customAlertView.h"

@implementation UIAlertController (customAlertView)
+ (void)showTipsAlertWithTitle:(NSString *)title subTitle:(NSString *)subTitle  cAction:(SEL)cAction userinfo:(NSDictionary *)userinfo  hasCancel:(BOOL)hasCancel addTarget:(UIViewController *)target{
//  UIViewController *rootViewController =  [UIApplication sharedApplication].keyWindow.rootViewController;
    UIAlertController *alertVC =[UIAlertController alertControllerWithTitle:title message:subTitle preferredStyle:UIAlertControllerStyleAlert];
//    __block UIViewController *selfVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    __block UIViewController *selfVC = target;
    __block SEL myAction = cAction;
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"好的" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * _Nonnull action) {
        if ( [selfVC respondsToSelector:cAction]) {
            [selfVC performSelector:cAction withObject:userinfo];
        }else{
            myAction = nil;
        }
        
    }];
    
    if (hasCancel) {
        UIAlertAction *cancelBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault  handler:nil];
        [alertVC addAction:cancelBtn];
    }
    [alertVC addAction:okBtn];
    
    [target presentViewController:alertVC animated:YES completion:nil];
}

+ (UIViewController *)currentViewController{
    UIViewController *resultVC = nil;
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for (UIWindow *tempWind in windows) {
            if (tempWind.windowLevel == UIWindowLevelNormal) {
                window = tempWind;
                break;
            }
        }
    }
    
    UIView *frontview = [[window subviews] objectAtIndex:0];
    id aNextReponser = [frontview nextResponder];
    
    if ([aNextReponser isKindOfClass:[UIViewController class]]) {
        resultVC = aNextReponser;
    }
    else {
        resultVC = window.rootViewController;
    }
    return resultVC;
}

@end
