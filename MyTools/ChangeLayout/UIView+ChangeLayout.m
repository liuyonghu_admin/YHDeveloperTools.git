//
//  UIView+ChangeLayout.m
//  zhangshao
//
//  Created by victorLiu on 2018/3/16.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import "UIView+ChangeLayout.h"

@implementation UIView (ChangeLayout)
- (void)changeLayoutWithType:(NSLayoutAttribute)layoutName constant:(CGFloat)constant{
    NSArray *constaintArray = self.constraints;
    for (NSInteger i = 0; i < constaintArray.count; i ++) {
        NSLayoutConstraint *constaint = constaintArray[i];
        if (constaint.firstAttribute == layoutName ) {
            constaint.constant = constant;
            [self.superview layoutIfNeeded];
        }
    }
}
@end
