//
//  NetHelper.m
//  client
//
//  Created by victorLiu on 2018/1/16.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import "NetHelper.h"
#import "NSString+MD5String.h"
#import "NSDictionary+allKeys.h"
#import "AFNetworking.h"
#import "NetRequsetParametersHeader.h"
#import "UIActivityIndicatorView+YHIicatorView.h"
#import "NSObject+ChangeObjectToDictionary.h"
#import "Paramters.h"
#import "NSObject+ParametersInitMethod.h"
@interface NetHelper ()
@property(nonatomic,strong)AFHTTPSessionManager *manager;
@property(nonatomic,strong)NSString *md5Sgin;
@property(strong,nonatomic)NSArray *netWotkTypeArray;

@end


@implementation NetHelper
//MARK:user states

@synthesize userOnLine =  _userOnLine,token = _token;
-(void)setUserOnLine:(BOOL)userOnLine{
    if (_userOnLine != userOnLine) {
        _userOnLine = userOnLine;
        [[NSUserDefaults standardUserDefaults] setBool:_userOnLine forKey:@"onLine"];
    }
}

-(BOOL)userOnLine{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"onLine"];
}
-(void)setToken:(NSString *)token{
    if (![_token isEqualToString:token]) {
        _token = token;
        [[NSUserDefaults standardUserDefaults] setObject:_token forKey:@"token"];
    }
}
-(NSString *)token{
     return [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
}

+(NetHelper *)defaultNetHepler{
    static  NetHelper *defualtNetWorkHelper;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        defualtNetWorkHelper = [[NetHelper alloc]init];
        defualtNetWorkHelper.md5Sgin = nil;
        defualtNetWorkHelper.manager = [AFHTTPSessionManager manager];
        defualtNetWorkHelper.manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        [defualtNetWorkHelper.manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        defualtNetWorkHelper.manager.requestSerializer.timeoutInterval = 5.f;
        defualtNetWorkHelper.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //        接口类型
        defualtNetWorkHelper.netWotkTypeArray = @[@"order/",@"message/",@"my/",@"login/",@"clientlogout"];
    });
    return defualtNetWorkHelper;
}

- (Paramters *)parameters{
    if (!_parameters) {
        _parameters  = [[Paramters alloc]init];
    }
    _parameters.token = [NetHelper defaultNetHepler].token;
    return _parameters;
}

- (void)checkOnlieStateBlock: (void(^)(BOOL onlie,NSDictionary *response))block{
    NetHelper *netHelper =[NetHelper defaultNetHepler];
    
    if (!netHelper.token) {
        block(NO,nil);
        return;
    }
    [[NetHelper defaultNetHepler] dataTaskWithMethod:@"post" netWorkType:login lastPathString:@"clientautoLogin" parameter:netHelper.parameters callBacKBlock:^(NSDictionary *reponseData, NSString *message) {
        if (reponseData != nil && [[reponseData objectForKey:@"codeId"] integerValue] == 0) {
            netHelper.token = [reponseData objectForKey:@"token"];
            block(YES,reponseData);
            
        }else{
            block(NO,nil);
        }
    }];
}


- (void)dataTaskWithMethod:(NSString *)method netWorkType:(netWorkType)netWorkType lastPathString:(NSString *)str parameter:(Paramters *)parameter callBacKBlock:(void(^)( NSDictionary *reponseData , NSString *message))block{
    NetHelper *defaultMananger =[NetHelper defaultNetHepler];
    //    拼接请求地址的路径
    NSString *urlString = customerUrl;
    if ([str isEqualToString:@"sendsms"]) {
        urlString = @"http://172.30.101.249:9095/app/driver/";
    }else{
        urlString  = [urlString stringByAppendingString:[defaultMananger.netWotkTypeArray objectAtIndex:netWorkType]];
    }
    urlString  = [urlString stringByAppendingString:str];
    
    //    判断请求类型
    method = [method uppercaseString];
    //    获取入参
    NSMutableDictionary *methodPar;
    if (parameter) {
         methodPar= [[NSDictionary changeToDictionaryWithObject:parameter] mutableCopy];
    }
    
    
    //    获取当前控制器显示加载
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [UIActivityIndicatorView showYHIndicatorViewWithTargetViewController:vc];
    if ([method isEqualToString:@"POST"]) {
        
        [defaultMananger.manager POST:urlString parameters:methodPar progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            //            隐藏加载
            
            [UIActivityIndicatorView hideYHIndicatorViewWithTargetViewController:vc];
            
            //            反序列化
            if (responseObject) {
                NSDictionary *resultDic =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                
                if (resultDic) {
                    //                    服务返回码
                    NSInteger resultCode = [[resultDic valueForKey:@"codeId"] integerValue];
                    //                    服务信息
                    NSString *message = [resultDic objectForKey:@"message"];
                    if (!resultCode) {
                        NSDictionary *resultBody = [resultDic objectForKey:@"resultBody"];
                        if (resultBody && ![resultBody isKindOfClass:[NSNull class]]) {
                            NSData *data = [NSJSONSerialization dataWithJSONObject:resultBody options:0 error:nil];
                            NSString *resultStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                            block(resultBody,[message stringByAppendingString:[NSString stringWithFormat:@"\n\nresilt message = %@",resultStr]]);
                        }else{
                            block(resultBody,message);
                        }
                        //                        NSLog(@"resultBody = %@",resultBody);
                    }else{
                        NSLog(@"resultCode error \n urlString =%@ \n resultCode =%@",urlString,[resultDic valueForKey:@"codeId"]);
                        
                        block(nil,message);
                    }
                }else{
                    NSLog(@"resultDic error urlString =%@",urlString);
                }
            }else{
                NSLog(@"responseObject error urlString =%@",urlString);
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //            隐藏加载
//            [NSThread sleepForTimeInterval:3];
            
            [UIActivityIndicatorView hideYHIndicatorViewWithTargetViewController:vc];
             block(nil,@"连接不上服务器！");
            NSLog(@"error.userinfo = %@ \n urlString =%@ \n",error.userInfo,urlString);
            
        }];
    }
    else if ([method isEqualToString:@"GET"]){
        [defaultMananger.manager GET:urlString parameters:methodPar progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            //            隐藏加载
//            [NSThread sleepForTimeInterval:3];
            [UIActivityIndicatorView hideYHIndicatorViewWithTargetViewController:vc];
            //            反序列化
            if (responseObject) {
                NSDictionary *resultDic =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                
                if (resultDic) {
                    //                    服务返回码
                    NSInteger resultCode = [[resultDic valueForKey:@"codeId"] integerValue];
                    //                    服务信息
                    NSString *message = [resultDic objectForKey:@"message"];
                    if (!resultCode) {
                        NSDictionary *resultBody = [resultDic objectForKey:@"resultBody"];
                        if ([message isEqualToString:@"OK"]) {
                            NSString *resultStr;
                            if (![resultBody isKindOfClass: [NSNull class]]) {
                                NSData *data = [NSJSONSerialization dataWithJSONObject:resultBody options:0 error:nil];
                               resultStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                            }
                          
                            block(resultBody,[message stringByAppendingString:[NSString stringWithFormat:@"\n\nresilt message = %@",resultStr]]);
                        }else{
                            NSLog(@"resultCode error \n urlString =%@ \n resultBody =%@",urlString,[resultDic valueForKey:@"resultBody"]);
                        }
                        //                        NSLog(@"resultBody = %@",resultBody);
                    }else{
                        NSLog(@"resultCode error \n urlString =%@ \n resultCode =%@",urlString,[resultDic valueForKey:@"codeId"]);
                        
                        block(nil,message);
                    }
                }else{
                    NSLog(@"resultDic error urlString =%@",urlString);
                }
            }else{
                NSLog(@"responseObject error urlString =%@",urlString);
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //            隐藏加载
//            [NSThread sleepForTimeInterval:3];
            
            [UIActivityIndicatorView hideYHIndicatorViewWithTargetViewController:vc];
              block(nil,@"连接不上服务器！");
            NSLog(@"error.userinfo = %@ \n urlString =%@ \n",error.userInfo,urlString);
            
        }];
    }else{
        NSLog(@"没有相应的请求类型 - method：%@",method);
    }
    
}























@end
