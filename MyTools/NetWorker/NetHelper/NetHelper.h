//
//  NetHelper.h
//  client
//
//  Created by victorLiu on 2018/1/16.
//  Copyright © 2018年 刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,netWorkType) {
    order = 0,
    message,
    my,
    login
    
};
@class Paramters;
@interface NetHelper : NSObject
@property(assign,nonatomic)BOOL userOnLine;
@property(nonatomic,strong)NSString *token;
@property(strong,nonatomic)Paramters *parameters;

+(NetHelper *)defaultNetHepler;

- (void)checkOnlieStateBlock: (void(^)(BOOL onlie,NSDictionary *response))block;

- (void)dataTaskWithMethod:(NSString *)method netWorkType:(netWorkType)netWorkType lastPathString:(NSString *)str parameter:(Paramters *)parameter callBacKBlock:(void(^)(  NSDictionary *reponseData, NSString *message))block;



@end
