//
//  LeftViewTextfiled.h
//  NewKroreaCards
//
//  Created by victorLiu on 2017/5/19.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YHLeftViewTextfiled : UITextField
@property(nonatomic,strong) UILabel *yhLeftView;
@property(nonatomic,strong) UIButton *yhRightView;
@property(nonatomic,strong)CALayer *bottomLine;
@end
