//
//  LeftViewTextfiled.m
//  NewKroreaCards
//
//  Created by victorLiu on 2017/5/19.
//  Copyright © 2017年  刘勇虎. All rights reserved.
//

#import "YHLeftViewTextfiled.h"

@implementation YHLeftViewTextfiled

-(void)awakeFromNib{
    [super awakeFromNib];
    
    CGRect frame = self.frame;
    
    
    _yhLeftView  = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 90, frame.size.height-20)];
    _yhLeftView.font = [UIFont systemFontOfSize:16];
    _yhLeftView.textAlignment = NSTextAlignmentRight;
    self.leftView = _yhLeftView ;
    _yhRightView  = [UIButton buttonWithType:UIButtonTypeSystem] ;
        [_yhRightView setFrame:CGRectMake(0, 5,frame.size.width/8,frame.size.height -20)];
    _yhRightView.layer.masksToBounds = YES;
    _yhRightView.layer.cornerRadius = 4;
    _yhRightView.layer.borderWidth = 0.5;
    _yhRightView.layer.borderColor = [UIColor blueColor].CGColor;
    
    self.rightView = [[UIView alloc]initWithFrame:CGRectMake(5, 0,frame.size.width/8 +10, frame.size.height)];
    [self.rightView addSubview:_yhRightView];
    
    _bottomLine = [[CALayer alloc]init];
    [_bottomLine setFrame:CGRectMake(_yhLeftView.frame.size.width, frame.size.height -12.f, frame.size.width -_yhLeftView.frame.size.width, 0.5f)];
    _bottomLine.backgroundColor = [UIColor grayColor].CGColor;
    [self.layer addSublayer:_bottomLine];
    //    self.rightView = _yhRightView;
    //    self.rightViewMode = UITextFieldViewModeAlways;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
