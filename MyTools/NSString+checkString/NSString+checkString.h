//
//  NSString+checkString.h
//  境外易购
//
//  Created by victorLiu on 2017/11/8.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, CheckStringType) {
    Phone = 0,
    Email,
    Passworld,
    Account
};
@interface NSString (checkString)

+ (void)checkStringWithType:(CheckStringType)type andTargetString:(NSString *)targetString resultBlock:(void(^)(BOOL result , NSString *message))resultBlock;
@end
