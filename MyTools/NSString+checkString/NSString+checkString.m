//
//  NSString+checkString.m
//  境外易购
//
//  Created by victorLiu on 2017/11/8.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//




#import "NSString+checkString.h"


@implementation NSString (checkString)
+ (void)checkStringWithType:( CheckStringType)type andTargetString:(NSString *)targetString resultBlock:(void(^)(BOOL result , NSString *message))resultBlock{
    
    if (![targetString containsString:@" "] && ![targetString isEqualToString:@""]) {
        
        NSInteger stringLength =targetString.length;
        switch (type) {
            case Phone:{
                NSLog(@"phone");
                NSString *pattern = @"[^0-9]";
                NSRegularExpression *regularExpress = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
                NSInteger number   = [regularExpress numberOfMatchesInString:targetString options:0 range:NSMakeRange(0,stringLength)];
                
                
                if ( stringLength != 11 ) {
                    resultBlock(NO,@"手机号格式有误！");
                    return;
                }
                else if (number) {
                    resultBlock(NO,@"输入必须为纯数字！");
                    return;
                }
                resultBlock(YES,@"验证成功！");
            }
                break;
            case Email:{
                NSLog(@"Email");
                NSString *pattern = @"^[A-Za-z0-9\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
//  NSString *pattern = @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
                
                NSRegularExpression *regularExpress = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
                NSInteger number = 0;
                NSArray *matchArr = [regularExpress matchesInString:targetString options:0 range:NSMakeRange(0, stringLength)];
//                NSLog(@" %@   %ld",matchArr,matchArr.count);
                //               MARK:获取结果
                for (NSTextCheckingResult *result in matchArr) {
//                NSTextCheckingResult *result =  matchArr[0];
                    NSLog(@"%@ %@",NSStringFromRange(result.range),[targetString substringWithRange:result.range]);
                    number =result.range.length;
                }
                if (number != stringLength) {
                    resultBlock(NO,@"邮箱格式有误！");
                    return;
                }
                
                resultBlock(YES,@"验证成功！");
            }
                break;
            case Passworld:{
                NSLog(@"Passworld");
                resultBlock(YES,@"验证成功！");
            }
                break;
            case Account:{
                NSLog(@"Account");
                resultBlock(YES,@"验证成功！");
            }
                break;
            default:{
                resultBlock(NO,@"不支持此type类型的判断！");
            }
                break;
        }
        
    }
    else {
        resultBlock(NO,@"输入中有空格！");
    }
}

@end
