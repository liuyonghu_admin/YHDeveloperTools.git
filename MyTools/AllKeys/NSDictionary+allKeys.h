//
//  NSDictionary+allKeys.h
//  境外易购
//
//  Created by victorLiu on 2017/11/13.
//  Copyright © 2017年 刘勇虎. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (allKeys)

+ (NSMutableArray *)YHAllKeysWithDictionary:(NSDictionary *)dictionary;
+ (NSMutableArray *)YHValueArraryWithDictionary:(NSDictionary *)dictionary;
//获取排序后的字符串
+ (NSString  *)YHSortedStringWithDictionary:(NSDictionary *)dictionary;
@end
